# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_sqlite_session',
  :secret      => 'cd100e5f83cafaec2ed5dcae567a5754b2491b89a004d5870ef7c40c3b19600a3f7e63a0d23536ace8ef95ea26035590b00098bd32ba0d922c04d1b614a8cc19'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
